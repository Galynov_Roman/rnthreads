import { self } from 'react-native-threads';
import './config';

let count = 0;

self.onmessage = message => {
  console.tron.log(`THREAD: got message ${message}`);

  count++;
  for(let i = 0; i < 100000; i++) {
    console.log('work...')
    // self.postMessage(`Message #${i} from worker thread!`);
  }

  self.postMessage(`Message #${count} from worker thread!`);
}

