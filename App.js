import React, { Component } from 'react';
import {
  View,
} from 'react-native';
// import { RealmProvider } from 'react-native-realm';
import { StackNavigator } from 'react-navigation';
import LauncheScreen from './src/screens/LauncheScreen';
import SecondScreen from './src/screens/SecondScreen';
// import realm from './src/realmSchems/dataSchema';

const RootStack = StackNavigator({
  Home: {
    screen: LauncheScreen,
    title: 'Home Screen',
  },
  SecondScreen: {
    screen: SecondScreen,
    title: 'Second Screen',
  },
});

class App extends Component {
  render() {
    return (
      // <RealmProvider realm={realm}>
        <RootStack />
      // </RealmProvider>
    );
  }
}

export default App;
