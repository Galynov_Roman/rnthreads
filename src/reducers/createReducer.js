import { combineReducers } from 'redux';
import dataReducer from './dataReducer';

const reducers = {
  data: dataReducer,
};

const appReducer = combineReducers(reducers);
const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
