import * as types from '../actions/data';

const initialState = {
  data: [],
};

export default function dataReducer(state = initialState, action) {
  switch (action.type) {
    case types.SET_DATA:
      return {
        ...state,
        data: action.data,
      };
    default:
      return state;
  }
}
