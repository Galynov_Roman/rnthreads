import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';

// import { connect } from 'react-redux';
// import { connectRealm } from 'react-native-realm';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});

class SecondScreen extends React.Component {
  static propTypes = {
  };
  static defaultProps = {
  };

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Second Screen</Text>
      </View>
    );
  }
}

export default SecondScreen;
