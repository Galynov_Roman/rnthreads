import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  Button,
} from 'react-native';

import { Thread } from 'react-native-threads';

let count = 0;

const styles = StyleSheet.create({
  headerLeft: {
    padding: 7,
  },
});

class LauncheScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Home Screen',
    headerTitleStyle: styles.headerTitleStyle,
    headerRight: (
      <TouchableOpacity
        style={styles.headerLeft}
        onPress={() => navigation.navigate('SecondScreen')}
      >
        <Text>Second</Text>
      </TouchableOpacity>
    ),
  });

  static propTypes = {
  };
  static defaultProps = {
  };

  state = { messages: [] }

  workerThread = null;

  componentDidMount() {
    this.workerThread = new Thread('./worker.thread.js');
    this.workerThread.onmessage = this.handleMessage;
  }

  startWork = () => {
    for(let i = 0; i < 100000; i++) {
      console.log('work...')
    }
    count++;
    this.setState(state => {
      return { messages: [...state.messages, `Message #${count} from UI thread!`] };
    });
  }

  componentWillUnmount() {
    this.workerThread.terminate();
    this.workerThread = null;
  }

  handleMessage = message => {
    console.tron.log(`APP: got message ${message}`);

    this.setState(state => {
      return { messages: [...state.messages, message] };
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native Threads!
        </Text>

        <Button 
          title="Start work in UI Thread!" 
          onPress={() => this.startWork()}
        />

        <Button 
          title="Send Message To Worker Thread" 
          onPress={() => {this.workerThread.postMessage('Hello')}} 
        />

        <View>
          <Text>Messages:</Text>
          {this.state.messages.map((message, i) => <Text key={i}>{message}</Text>)}
      </View>
      </View>
    );
  }
}

export default LauncheScreen;
