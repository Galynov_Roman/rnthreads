export const SET_DATA = 'SET_DATA';

export const setData = data => ({
  type: SET_DATA,
  data,
});

export const testThunk = () => (dispatch, getState) => {
  console.warn('enter to test function, data', getState().data);
};
